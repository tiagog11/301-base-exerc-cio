FROM openjdk:8-jdk-alpine AS JDK
COPY ./ ./
RUN ./mvnw clean package -DskipTests

FROM openjdk:8-jre-alpine
WORKDIR /app
ARG JAR_FILE=target/*.jar 
COPY --from=JDK ${JAR_FILE} .
CMD ["java","-jar","Api-Investimentos-0.0.1-SNAPSHOT.jar"]